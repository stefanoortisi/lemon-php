<?php
	define('PATH_ROOT', dirname(__FILE__));
	
	
	
	// require_once dirname(__FILE__) . '/config/default/config.inc.php';
	if (file_exists(dirname(__FILE__) . '/config/' . $_SERVER['HTTP_HOST'] . '/config.inc.php')) {
		require_once dirname(__FILE__) . '/config/' . $_SERVER['HTTP_HOST'] . '/config.inc.php';
	} else {
		require_once dirname(__FILE__) . '/config/default/config.inc.php';
	}
	        
	foreach (glob(dirname(__FILE__) . '/lib/*.inc.php') as $lib) {
		require_once $lib;
	}
	
	$rawRequest = json_decode(file_get_contents('php://input'), true); 