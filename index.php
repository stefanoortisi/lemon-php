<?php
	require 'bootstrap.inc.php';
	
	session_start();
	ob_start();
	
	$q = (!empty($_GET['q'])) ? $_GET['q'] : NULL;
	
	$controller = 'main';
	$action = 'ctlmain';
	$args = array();
	
	global $response_format;
	$response_format = 'html';
	
	define('POSTBACK', (!empty($_POST)) ? true : false);
	
	if (!empty($q)) {
		$req = explode('/', $q);
		if (!empty($req[0])) $controller = $req[0];
		if (!empty($req[1])) $action = $req[1];
		if (count($req) > 2) { $args = array_slice($req, 2); }
		$args = array_merge($args, $_GET);
		if (POSTBACK) { $args = array_merge($args, $_POST); }
		if (strpos($action, '.') !== false) {
			$actPc = explode('.', $action, 2);
			$action = $actPc[0];
			$response_format = $actPc[1];
		}
	}
	
	$ctlFile = dirname(__FILE__) . '/controllers/controller.' . preg_replace("/[^a-zA-Z0-9]/", "", $controller) . '.php';
	if (file_exists($ctlFile)) {
		require $ctlFile;
		if (function_exists($controller . '_' . $action)) {
			call_user_func($controller . '_' . $action, $args);
		}
	}
	
	