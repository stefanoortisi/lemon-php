<?php
	function send_response($data=NULL, $status=200, $error=NULL, $format=NULL) {
		$out = array(
				'status' => ($status==200) ? 'OK' : 'KO',
				'error'	=> $error,
				'timestamp' => time(),
				'data' => $data
			);
		
		if (empty($format)) {
			global $response_format;
			$format = $response_format;
		}
		
		send_status($status);
		if (function_exists('send_' . $format)) {
			call_user_func('send_' . $format, $out);
		}
		
		exit();
	}
	
	function send_json($out) {
		header('Content-Type: application/json');
		print json_encode($out);
	}
	
	function send_xml($out) {
		print '<Response>';
		print '<Status>' . $out['status'] . '</Status>';
		print '<Error><[CDATA[' . $out['error'] . ']]></Error>';
		print '<Timestamp>' . $out['timestamp'] . '</Timestamp>';
		print '<Data><[CDATA[' . $out['data'] . ']]></Data>';
		print '</Response>';
	}
	
	function send_html($out) {
		print '<html><body>';
		print (!empty($out['error'])) ? $out['error'] : $out['data'];
		print '</body></html>';
	}
	
	function send_text($out) {
		print (!empty($out['error'])) ? $out['error'] : $out['data'];
	}
	
	function send_status($status_code=200) {
		switch ($status_code) {
			case 200:
				header('HTTP/1.1 200 OK');
				break;
			case 404:
				header('HTTP/1.1 404 Not Found');
				break;
			case 403:
				header('HTTP/1.1 403 Forbidden');
				break;
			case 401:
				header('HTTP/1.1 401 Authorization Required');
				break;
			default:
				header('HTTP/1.1 500 Internal Server Error');
		}
	}
	
	function send_message($message, $class='confirm') {
		$_SESSION['_msg_' . $class] = $message;
	}
	
	function send_confirm($message) {
		send_message($message, 'confirm');
	}
	
	function send_warning($message) {
		send_message($message, 'warning');
	}
	
	function send_error($message) {
		send_message($message, 'error');
	}