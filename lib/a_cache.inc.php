<?php
	/*
		define CACHE_PATH and CACHE_NEWS_MAX_AGE
	*/
	
	function cache_set($key, &$value){
		$path = CACHE_PATH . '/' . cache_sanitize_key($key);
		if(file_exists($path)){
			unlink($path);
		}
		
		file_put_contents($path, serialize($value));
	}
	
	function cache_get($key, $max_age=NULL) {
		if (empty($max_age)) {
			$max_age = CACHE_NEWS_MAX_AGE;
		}
		$path = CACHE_PATH . '/' . cache_sanitize_key($key);
		if (file_exists($path) && (time() - filemtime($path)) < $max_age){
			return unserialize(file_get_contents($path));
		} else {
			cache_flush(basename($path));
		}
		
		return false;
	}
	
	function cache_sanitize_key($key) {
		return preg_replace("/[^a-zA-Z0-9_]/", "", $key);
	}
	
	function cache_flush($pattern='*') {
		$cached = glob(CACHE_PATH . '/' . $pattern);
		if (!empty($cached)) {
			foreach ($cached as $cache_element) {
				unlink($cache_element);
			}
		}
	}