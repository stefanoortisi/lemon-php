<?php
function bat_is_local(){
	return $_SERVER[ 'HTTP_HOST' ] == 'batpat.stefano.loc';
}

if( bat_is_local() ){
	require_once('FirePHPCore/FirePHP.class.php');
}

function controller_log( $msg, $label ){
	if( bat_is_local() ){
		ob_start();
		$firephp = FirePHP::getInstance(true);
		if( $label )
			$firephp->log( $msg, $label );
		else
			$firephp->log( $msg );
		ob_clean();
	}	
}


/*
 * Shortcuts
 */
 
function _log( $msg, $label = NULL ){
	controller_log( $msg, $label );
}

?>