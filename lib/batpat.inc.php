<?php 
define( 'MONO_LANG', false );

define( 'BAT_DEFAULT_LANG', 'en' );
define( 'BAT_LANG', bat_read_lang() );
define( 'BAT_URL_LANG', bat_url_lang() );
define( 'THEME_URL', 'http://' . $_SERVER[ 'HTTP_HOST' ] . '/assets' );	
define( 'BASE_URL', 'http://' . $_SERVER[ 'HTTP_HOST' ] . BAT_URL_LANG . '/home' );
define( 'BASE_URL_BLOG', 'http://' . $_SERVER[ 'HTTP_HOST' ] . BAT_URL_LANG . '/blog' );
define( 'BASE_PATH', dirname( dirname( dirname( __FILE__ ) ) ) );
define( 'BASE_WP_PATH', BASE_PATH . '/blog'  );
define( 'DEFAULT_THUMB_CARTOON', THEME_URL . '/js/jwplayer/default_thumb.jpg' );
define( 'POST_PER_PAGE', 2 );
define( 'MAX_PAGES_PAGINATION', 9 );
define( 'BLOG_VIDEO_WIDTH', 538 );
define( 'BLOG_VIDEO_HEIGHT', 372 );
define( 'MTD_PARALLAX', use_mtd_parallax() );
define( 'DEBUG_PARALLAX', debug_parallax() );
define( 'AUTO_DETECT_LANG', true );
define( 'BITLY_KEY', 'R_3c5c5e83dc772c34d9c9d2cec525990e' );

function bat_url_lang(){
	if( MONO_LANG ){
		return '';
	}
	return '/' . BAT_LANG;
}

function debug_parallax(){
	if( !empty( $_GET[ 'd' ] ) && $_GET[ 'd' ] == 1 ){
		return true;
	}
	return false;
}

function use_mtd_parallax(){
	if( !empty( $_GET[ 'mtd' ] ) && $_GET[ 'mtd' ] == 1 ){
		return true;
	}
	if( ios_detect() != 'default' && !ios_5_detect() ){
		return true;
	}
	if( bat_is_android() || bat_is_iphone() ){
		return true;
	}
	return false;
}

function bat_is_android(){	
	return (bool) strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'android');
}

function detect_android(){
	if( bat_is_android() ){
		return 'android';
	}
	return '';
}

function is_windows(){
	return strpos(  strtolower( $_SERVER[ 'HTTP_USER_AGENT' ] ), 'windows' ) !== false;
}

function is_facebook(){
	return strpos(  strtolower( $_SERVER[ 'HTTP_USER_AGENT' ] ), 'facebook' ) !== false;
}

function detect_os(){
	if( is_windows() ){
		return 'windows';
	} else {
		return 'nowindows';
	}
}

function detect_fake_parallax(){
	if( MTD_PARALLAX ){
		return 'old_ios';
	} else {
		return '';
	}
}

function bat_game_active(){
	return $platform[ 0 ] != 'android';
	/*
$platform = _get_platform();
	
	return $platform[ 0 ] == 'desktop';
*/
}

function bat_blog_active(){
	switch( BAT_LANG ){
		case 'es':
		case 'ca':
			return true;
		break;
		default:
			return false;
	}
}

function bat_cartoons_active(){
	return true;

	
	// switch( BAT_LANG ){
	// 	case 'es':
	// 		return false;
	// 	break;
	// 	default:
	// 		return true;
	// }
}

function bat_is_iphone(){
	$isiPhone = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPhone');
	$isiPod = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPod');
	return $isiPhone || $isiPod;
}

function _get_platform(){
		$platforms = array( 
			array( 'desktop', 'flash_game.jpg' ),
			array( 'apple', 'flash_game_mobile.jpg' ),
			array( 'android', 'flash_game_mobile.jpg' )
		);


		//return $platforms[ 1 ];
		
		$isiPad = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPad');
		$isiPhone = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPhone');
		$isiPod = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPod');
		$isAndroid = (bool) strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'android');
		if( $isiPad || $isiPhone || $isiPod){
			return $platforms[ 1 ];
		}
		if( $isAndroid ){
			return $platforms[ 2 ];
		}
		
		
		return $platforms[ 0 ];
	}


/**
* PAGE ID DEFINE (unused)
*/
define( 'SHARE_INFO_ID', 4 );

function bat_read_lang(){
	$temp = explode( '/', $_SERVER[ 'REQUEST_URI' ] );

	switch( $temp[ 1 ] ){
		case 'en':
		case 'es':
		case 'ca':
		case 'fr':
		case 'it':
			return $temp[ 1 ];
		break;
		default:
			
			return BAT_DEFAULT_LANG;
	}
}

function ios_detect() {
	$browser = strtolower($_SERVER['HTTP_USER_AGENT']); // Checks the user agent
	
	if(strstr($browser, 'iphone') || strstr($browser, 'ipod')) {
	  	$device = 'iphone';
	} else if(strstr($browser, 'ipad')){
		$device = 'ipad';
	} else { $device = 'default'; }	
	return($device);
}

function ios_5_detect(){
	$browser = strtolower($_SERVER['HTTP_USER_AGENT']); // Checks the user agent
	if(strstr($browser, 'os 5_' )){
		return true;
	} else {
		return false;
	}
}

function can_use_parallax(){
	if( ios_5_detect() ){
		return true;
	} else {
		return false;
	}
}


function check_auto( $a ){
	if( $a == 'auto' ){
		return $a;
	} else {
		return $a . 'px';
	}
}	
/****************************************************
* SHARE SECTION
*****************************************************/
function bat_get_share_info( $id ){
	$share_info = batpat_get_share_info();
	if( !empty( $share_info[ $id ] ) ){
		return array(
			'id' => $share_info[ $id ][ 'id' ],
			'title' => !empty($share_info[ $id ][ 'title' ]) ? $share_info[ $id ][ 'title' ] : $share_info[ 'homepage' ][ 'title' ],
			'description' => !empty($share_info[ $id ][ 'description' ]) ? $share_info[ $id ][ 'description' ] : $share_info[ 'homepage' ][ 'description' ],
			'image' => !empty($share_info[ $id ][ 'image' ]) ? $share_info[ $id ][ 'image' ] : $share_info[ 'homepage' ][ 'image' ],
			'thumb' => !empty($share_info[ $id ][ 'thumb' ]) ? $share_info[ $id ][ 'thumb' ] : $share_info[ 'homepage' ][ 'thumb' ]
		);
	}
	return $share_info[ 'homepage' ];
	
}

function bat_check_facebook(){
	if( !is_facebook() ){
		$page = '';
		if( strpos(  strtolower( $_SERVER[ 'REQUEST_URI' ] ), 'downloads' ) !== false ){
			$page = 'downloads';
		} else if( strpos(  strtolower( $_SERVER[ 'REQUEST_URI' ] ), 'cartoons' ) !== false ){
			$page = 'cartoons';
		} else if( strpos(  strtolower( $_SERVER[ 'REQUEST_URI' ] ), 'cards' ) !== false ){
			$page = 'cards';
		} else if( strpos(  strtolower( $_SERVER[ 'REQUEST_URI' ] ), 'games' ) !== false && empty( $_GET[ 'overlay' ])){
			$page = 'game';
		}
		
		if( strlen( $page ) > 1  ){
			header( "Location: http://" . $_SERVER[ 'HTTP_HOST' ] . '/' . BAT_LANG . '/home/manor#' . $page );
			exit();
		}
	}
}


function batpat_get_share_info(){
	$size = array("h" => 308, "w" => 1099, "zc" => 1, "q" =>100);
	$args = array( 
		'post_type' => 'share_info', 
	);	
	$my_query = new WP_Query( $args ); 
	
	$output = unserialize( cache_get( 'SHARE_INFO_' . BAT_LANG ) );
	if( !$output ){
		$output = array();
		while ($my_query->have_posts()) : $my_query->the_post(); 
			$pages = get_group('Page');
			foreach( $pages as $page ){
				$page_id = $page[ 'page_id' ][ 1 ];
				$output[ $page_id ] = array(
					'id' => $page_id,
					'title' => $page[ 'page_title' ][ 1 ],
					'description' => $page[ 'page_description' ][ 1 ],
					'image' => $page[ 'page_image' ][ 1 ][ 'original' ],
					'thumb' => $page[ 'page_image' ][ 1 ][ 'thumb' ],
				);
			}
			
			
			//$output = get_image( 'event_page_header_image', 1, 1, 0, NULL, $size );
		endwhile;
	
		wp_reset_postdata();
		cache_set( 'SHARE_INFO_' . BAT_LANG , serialize( $output ) );
	} 
	return $output;
}

/****************************************************
* LIBRARY SECTION
*****************************************************/
	
function bat_get_book_info(){
	
		
	$args = array( 
		'post_type' => 'library', 
	);	
	$my_query = new WP_Query( $args ); 
	$output = array();
	
	while ($my_query->have_posts()) : $my_query->the_post(); 
		$output[ 'link_bol' ] = get( 'link_bol' );
		$output[ 'link_appstore' ] = get( 'link_app_store' );
		$output[ 'books' ] = array();
		
		$books = get_group('Book');
		
		foreach( $books as $book ){
			$output[ 'books' ][] = array(
				'title' => $book[ 'book_title' ][ 1 ],
				'image' => $book[ 'book_preview' ][ 1 ][ 'thumb' ],
				'details' => !empty( $book[ 'book_gallery' ] ) ? 
					bat_get_gallery_from_book( $book[ 'book_gallery' ] ) : 
					false
			);
		}
		
		//$output = get_image( 'event_page_header_image', 1, 1, 0, NULL, $size );
	endwhile;

	wp_reset_postdata();

	
	return $output;
}	

function bat_get_gallery_from_book( $gallery ){
	$output = array();
	foreach( $gallery as $item ){
		$output[] = $item[ 'original' ];
	}
	return $output;
}

/****************************************************
* CARTOONS SECTION
*****************************************************/
function bat_get_cartoons(){
	$args = array( 
		'post_type' => 'cartoons', 
	);	
	$my_query = new WP_Query( $args ); 
	$output = array();
	while ($my_query->have_posts()) : $my_query->the_post(); 
		$cartoons = get_group('Cartoon');
		
		foreach( $cartoons as $cartoon ){
			$output[] = array(
				'title' => $cartoon[ 'cartoon_title' ][ 1 ],
				'abstract' => $cartoon[ 'cartoon_description' ][ 1 ],
				'video' => $cartoon[ 'cartoon_video' ][ 1 ],
				'preview' => !(empty($cartoon[ 'cartoon_preview_image' ])) ? $cartoon[ 'cartoon_preview_image' ][ 1 ][ 'original' ] : '',
				'thumb' => !(empty($cartoon[ 'cartoon_preview_image' ])) ? 
					$cartoon[ 'cartoon_preview_image' ][ 1 ][ 'thumb' ] : 
					DEFAULT_THUMB_CARTOON
			);			
		}
	endwhile;

	wp_reset_postdata();
	return $output;
}

/****************************************************
* GAME SECTION
*****************************************************/
function bat_game_info(){
	$args = array( 
		'post_type' => 'game', 
	);	
	$my_query = new WP_Query( $args ); 
	$output = array();
	while ($my_query->have_posts()) : $my_query->the_post(); 
		$output[ 'swf' ] = get( 'game_flash_swf' );
		$output[ 'window_title' ] = get( 'window_title' );
		$output[ 'title' ] = get( 'title' );
		$output[ 'image' ] = get( 'image' );
		$output[ 'description' ] = get( 'description' );
		$output[ 'lite_android' ] = get( 'link_android_lite' );
		$output[ 'full_android' ] = get( 'link_android_full' );
		$output[ 'lite_apple' ] = get( 'link_app_store_lite' );
		$output[ 'full_apple' ] = get( 'link_app_store_full' );
	endwhile;

	wp_reset_postdata();
	return $output;
}

/****************************************************
* DOWNLOADS SECTION
*****************************************************/
function bat_get_downloads(){
	$args = array( 
		'post_type' => 'downloads', 
	);	
	$my_query = new WP_Query( $args ); 
	$output = array();
	while ($my_query->have_posts()) : $my_query->the_post(); 
		$items = get_group('item');
		foreach( $items as $item ){
			$output[] = bat_get_single_downloads( $item );			
		}
		
	endwhile;

	wp_reset_postdata();
	return $output;
}


//******
function bat_get_single_downloads( $item ){
	$output = array(
		'thumb' => $item[ 'item_thumb' ][ 1 ][ 'original' ],
		'title' => $item[ 'item_title' ][ 1 ]
	);
	
	if( !empty( $item[ 'item_link' ] ) ){
		$output[ 'link' ] = $item[ 'item_link' ];
	} else {
		if( !empty( $item[ 'item_download_pc' ] ) ){
			$output[ 'links' ][ 'pc' ] = $item[ 'item_download_pc' ][ 1 ];
		}
		if( !empty( $item[ 'item_download_mac' ] ) ){
			$output[ 'links' ][ 'mac' ] = $item[ 'item_download_mac' ][ 1 ];
		}
		if( !empty( $item[ 'item_download_pc_mac' ] ) ){
			$output[ 'links' ][ 'pcmac' ] = $item[ 'item_download_pc_mac' ][ 1 ];
		}
	}
	
	
	return $output;
}

/****************************************************
* ECARDS SECTION
*****************************************************/
function bat_get_cards(){
	$args = array( 
		'post_type' => 'ecards', 
	);	
	$my_query = new WP_Query( $args ); 
	$output = array();
	while ($my_query->have_posts()) : $my_query->the_post(); 
		$cards = get_group('ecard');
		$output[ 'privacy' ] = get( 'privacy_policy' );
		foreach( $cards as $card ){
			$output[ 'cards' ][] = array(
				'web' => $card[ 'ecard_site_image' ][ 1 ][ 'thumb' ],
				'mail' => $card[ 'ecard_mail_image' ][ 1 ][ 'thumb' ],
			);
		}
	endwhile;
	wp_reset_postdata();
	return $output;
}

function bat_try_scroller(){
	return false;
}

function batpat_getmail(){
	$args = array( 
		'post_type' => 'general', 
	);	
	$my_query = new WP_Query( $args ); 
	$email = array();
	while ($my_query->have_posts()) : $my_query->the_post(); 
		$email = get_field('mail_contacts');
	endwhile;
	wp_reset_postdata();
	return $email[ 1 ];
}

function bat_get_links(){
	$args = array( 
		'post_type' => 'links', 
	);	
	$my_query = new WP_Query( $args ); 
	$output = array();
	while ($my_query->have_posts()) : $my_query->the_post(); 
		$links = get_group('Link');
		foreach( $links as $link ){
			$output[] = array(
				'label' => $link[ 'link_label' ][ 1 ],
				'link' => $link[ 'link_link' ][ 1 ],
			);
		}
	endwhile;
	wp_reset_postdata();
	return $output;
}

function bat_get_post_info(){
	global $post;
	$output = array();
	$output[ 'images' ] = get_field( 'images' );
	$player = get_group( 'video' );
	if( !empty( $player ) ){
		foreach( $player as $video ){
			$output[ 'player' ][] = array(
				'preview' => $video[ 'video_preview' ][ 1 ][ 'original' ],
				'video' => $video[ 'video_video' ][ 1 ]
			);
		}
	}
	
	$output[ 'youtubes' ] = get_field( 'youtube_video' );
	$output[ 'vimeos' ] = get_field( 'vimeo_video' );
	$output[ 'show_as_preview' ] = get( 'show_as_preview' );
	$output[ 'title' ] = get_the_title();
	$output[ 'description' ] = get_the_excerpt();
	$output[ 'link' ] = get_permalink();
	$output[ 'image_popup' ] = get_field( 'popup_image_popup' );
	$output[ 'date' ] = get_the_time();
	$output[ 'id' ] = get_the_ID();
	
	return $output;
}

function bat_get_news(){	
	
	$to_popup = cache_get( 'BAT_GET_NEWS' );
	if( !$to_popup ){
		
		$args = array( 
			'post_type' => 'post', 
		);	
		$my_query = new WP_Query( $args ); 
		
		$time = -1;
		while ($my_query->have_posts()) : $my_query->the_post(); 
			if( get( 'popup_activate_popup' ) == 1 ){
				$temp = bat_get_post_info();
				if( $time < $temp[ 'date' ] ){
					$time = $temp[ 'date' ];
					$to_popup = $temp;
				}
			}
		endwhile;
		wp_reset_postdata();
		cache_set( 'BAT_GET_NEWS', $to_popup );	
	} else {
		//cache found	
	}
	
	return $to_popup;
}

function print_analytics(){
	echo "<script type=\"text/javascript\">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-5817103-40']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>";
}

function bat_is_wp(){
	$temp = explode( '/', $_SERVER[ 'REQUEST_URI' ] );
	if( !empty( $temp[ 2 ] ) && $temp[ 2 ] == 'home' || 
		!empty( $temp[ 1 ] ) && $temp[ 1 ] == 'home'){
		return false;
	}
	return true;
}

function get_lang_fb(){
	switch( BAT_LANG ){
		case 'ca':
			return 'ca_ES';
		break;
		case 'es':
			return 'es_ES';
		break;
		case 'fr':
			return 'fr_FR';
		break;
		case 'it':
			return 'it_IT';
		break;
		case 'en':
		default:
			return 'en_US';
	}
}

function get_width_like(){
	switch( BAT_LANG ){
		case 'ca':
		case 'es':
		case 'it':
			return 110;
		break;
		case 'en':
		case 'fr':
		default:
			return 75;
	}
}

function l_image( $image ){
	return '/locale/' . get_lang_fb() . '/images/' . $image;
}

function bat_thumb( $src ){
	$output = PT();
	$output .= "?src=$src&w=130&h=90";
	return $output;
}

function _get_theme(){
	$time = array( 'ngt', 'day');
	$hour = bat_get_locale_time();
	
	if( $hour <= 8 || $hour >= 18 ){
		return $time[ 0 ];
	} else {
		return $time[ 1 ];
	}
}

function bat_get_locale_time(){
	if( !empty( $_COOKIE['BATPAT_COOKIE' ] ) ){
		$fuse = substr($_COOKIE['BATPAT_COOKIE' ], 1);
		$fuse = intval( substr($fuse, 0, 3) );
		$offset=$fuse*60*60;
		$dateFormat="H";
		$hour = gmdate($dateFormat, time()+$offset);
		return $hour;
	}
	return false;
}

function bat_get_db_name(){
	$output;
	if(  $_SERVER[ 'HTTP_HOST' ] == 'batpat.atlantyca.com' ){
		switch( BAT_LANG ){
			case 'it':
				$output = 'atlantyca_com_batpat_it';
			break;
			case 'es':
				$output = 'atlantyca_com_batpat_es';
			break;
			case 'ca':
				$output = 'atlantyca_com_batpat_ca';
			break;
			case 'fr':
				$output = 'atlantyca_com_batpat_fr';
			break;
			case 'en':
			default:
				$output = 'atlantyca_com_batpat_en';
			
		}	
	} else {
		switch( BAT_LANG ){
			case 'it':
				$output = 'DEV_batpat_it';
			break;
			case 'es':
				$output = 'DEV_batpat_es';
			break;
			case 'ca':
				$output = 'DEV_batpat_ca';
			break;
			case 'fr':
				$output = 'DEV_batpat_fr';
			break;
			case 'en':
			default:
				$output = 'DEV_batpat_en';
			break;
		}	
	}
	
	return $output;
}

function bat_get_info_db(){
	$output = array(
		'db_name' => '',
		'db_password' => '',
		'db_host' => '',
		'db_user' => ''
	);
	switch( $_SERVER[ 'HTTP_HOST' ] ){
		case 'batpat.atlantyca.com':
			$output = array(
				'db_name' => bat_get_db_name(),
				'db_password' => 'BatpatS1to',
				'db_host' => 'hostingmysql238.register.it',
				'db_user' => 'PM6930_batpat'
			);
		break;
		default:
			$output = array(
				'db_name' => bat_get_db_name(),
				'db_password' => 'mut4do)',
				'db_host' => 'localhost',
				'db_user' => 'root'
			);
	}

	return $output;
}

function bat_set_locale(){
	$lang = BAT_LANG;
	$locale = get_lang_fb();	
	putenv('LC_ALL=' . $locale . '.UTF-8' );
	setlocale(LC_ALL, $locale . '.UTF-8' );
	bindtextdomain("messages", dirname( dirname( dirname( __FILE__ ) ) ) . "/locale");
	textdomain("messages");
}

function llog( $string ){
	if( !empty( $_GET[ 'debug' ] ) ){
		echo '<p style="color:white">' . $string . '</p>';
		var_dump( $string );
	}
}

function CompressURL($url) {

  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, "http://to.ly/api.php?longurl=".urlencode($url));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
  curl_setopt($ch, CURLOPT_HEADER, 0);

  $shorturl = curl_exec ($ch);
  curl_close ($ch);

  return $shorturl;

}

function getShortUrl( $url ){
	$tinyUrl = cache_get( md5($url ) );
	if( !$tinyUrl ){
		$tinyUrl = CompressURL( $url );
		cache_set( md5( $url ), $tinyUrl );
	}
	return $tinyUrl;
}

//bat_check_lang_url();
bat_set_locale();

//override db name
global $db_name;
$db_name = bat_get_db_name();

if( !bat_is_wp() ){
	
	//require_once( dirname(__FILE__) . '/../../blog/wp-blog-header.php' );
	require(dirname(__FILE__) . '/../../blog/wp-config.php');
	$wp->init();
	$wp->parse_request();
	$wp->query_posts();
	$wp->register_globals();
	
	
	if( !MONO_LANG && AUTO_DETECT_LANG && !is_facebook() ){
		session_start();
		if( empty( $_SESSION[ 'BATPAT_FAVOURITE_LANG' ] ) ){
			detect_favourite_lang();
		} 
	}
}

?>