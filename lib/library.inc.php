<?php
define( 'LIBRARY_TYPE', 'library_type' );
define( 'CACHE_LIBRARY', TRUE ); 

function library_is_new_version(){
	$activate = cache_get( BAT_LANG . '_library_new', 60 );
	if( !CACHE_LIBRARY || !$activate ){
		$args = array( 
			'post_type' => 'general', 
		);	
		$my_query = new WP_Query( $args ); 
		$activate = array();
		while ($my_query->have_posts()) : $my_query->the_post(); 
			$activate = get_field('activate_library_second_level');
		endwhile;
		wp_reset_postdata();	
		
		cache_set( BAT_LANG . '_library_new', $activate );
	}
	
	
	if( !empty( $activate[ 1 ] ) && $activate[ 1 ] == 1 ){
		return true;
	} else {
		return false;
	}
}

function library_get_categories(){
	$terms = unserialize( cache_get( BAT_LANG . '_terms_library', 10*60 ) );
	if( !CACHE_LIBRARY || !$terms ){
		$terms = apply_filters( 'taxonomy-images-get-terms', '', array( 'taxonomy' => LIBRARY_TYPE ) );
		
		foreach( $terms as $term ){
			$image_url = wp_get_attachment_image_src($term->image_id, 'full', true);
			$term->image = $image_url[ 0 ];
		}
		
		cache_set( BAT_LANG . '_terms_library', serialize( $terms ) );
	}
	
	return $terms;
}

function library_get_posts_by_category( $cat ){
	$posts = unserialize( cache_get( BAT_LANG . '_terms_library_' . $cat , 10*60 ) );
	if( !CACHE_LIBRARY || !$posts ){
		$query = new WP_Query( array( LIBRARY_TYPE => $cat ) );
		$posts = array();
		$count = 0;
		while ($query->have_posts()) : $query->the_post(); 
			
			$posts[ $count ] = array(
				'title' => get_the_title(),
				'image' => get_image( 'image_preview', 1, 1, 0 ),
				'dropdown_active' => get( 'show_buttons_as_dropdown' ) == 1 ? true : false
			);
			
			//get carousel images
			$carousel = get_order_field( 'Carousel' );
			$posts[ $count ][ 'details' ] = array();
			foreach($carousel as $index){
				$posts[ $count ][ 'details' ][] = get( 'Carousel', 1, $index );
			}
			if( strlen( $posts[ $count ][ 'details' ][ 0 ] ) < 3 ){
				$posts[ $count ][ 'details' ] = false;	
			}
			
			//get buttons
			$posts[ $count ][ 'buttons' ] = array();
			
			$buttons = get_group( 'Button' );
			foreach( $buttons as $button ){
				if( !empty( $button[ 'button_title' ] ) ){
					$posts[ $count ][ 'buttons' ][] = array(
						'title' => $button[ 'button_title' ][ 1 ],
						'link' => $button[ 'button_link' ][ 1 ]
						/* ,'icon_type' => strtolower( $button[ 'button_icon_type' ][ 1 ] ) */
					);
				} else {
					$posts[ $count ][ 'buttons' ] = false;	
				}
				
			}
			
			$count++;

		endwhile;
	
		wp_reset_postdata();	
		
		cache_set( BAT_LANG . '_terms_library_' . $cat, serialize( $posts ) );	
	}
	/*

	echo "<pre>";
	print_r( $posts );
	echo "</pre>";
	die();
*/
	return $posts;
}

function bat_dump( $str ){
	echo "<pre>";
	print_r( $str );
	echo "</pre>";
}

?>