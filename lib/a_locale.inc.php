<?php
function detect_favourite_lang(){

	// $state = cache_get( 'IP_' . $_SERVER[ 'REMOTE_ADDR' ], CACHE_IP_MAX_AGE );
	// if( !$state ){
	// 	$state = strtolower( detect_city( $_SERVER[ 'REMOTE_ADDR' ] ) );	
	// 	cache_set( 'IP_' . $_SERVER[ 'REMOTE_ADDR' ], $state );	
	// }

    $state = strtolower( detect_city( $_SERVER[ 'REMOTE_ADDR' ] ) );    

	$state = trim($state);
	switch( $state ){
		case 'it':
		case 'es':
		case 'fr':
		case 'en':
        case 'ca':
			$lang = $state;
		break;
		case 'us':
			$lang = 'en';
		break;
		default:
			$lang = 'en';
		break;
	}
	switch_lang( $lang );
}

function switch_lang( $lang ){
	$_SESSION[ 'BATPAT_FAVOURITE_LANG' ] = $lang;
	//var_dump("Location: /$lang/" . substr( $_SERVER[ 'REQUEST_URI' ], 4 ) );
	
	if( substr( $_SERVER[ 'REQUEST_URI' ], 1, 2 ) != $lang ){
		header( "Location: /$lang/" . substr( $_SERVER[ 'REQUEST_URI' ], 4 )  );
	}
}

function detect_city($ip) {
		//TEMP
		//$ip = '93.62.0.51';
        $default = 'UNKNOWN';

        if (!is_string($ip) || strlen($ip) < 1 || $ip == '127.0.0.1' || $ip == 'localhost')
            $ip = '8.8.8.8';

        $curlopt_useragent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6 (.NET CLR 3.5.30729)';
		
        $url = 'http://ipinfodb.com/ip_locator.php?ip=' . urlencode($ip);
        $ch = curl_init();

        $curl_opt = array(
            CURLOPT_FOLLOWLOCATION  => 1,
            CURLOPT_HEADER      => 0,
            CURLOPT_RETURNTRANSFER  => 1,
            CURLOPT_USERAGENT   => $curlopt_useragent,
            CURLOPT_URL       => $url,
            CURLOPT_TIMEOUT         => 1,
            CURLOPT_REFERER         => 'http://' . $_SERVER['HTTP_HOST'],
        );

        curl_setopt_array($ch, $curl_opt);

        $content = curl_exec($ch);

        if (!is_null($curl_info)) {
            $curl_info = curl_getinfo($ch);
        }

        curl_close($ch);

        if ( preg_match('{<li>City : ([^<]*)</li>}i', $content, $regs) )  {
            $city = $regs[1];
        } 
        if ( preg_match('{<li>Country : ([^<]*)<img}i', $content, $regs) )  {
            $state = $regs[1];
        }

        if( $city!='' && $state!='' ){
          $location = $city . ', ' . $state;
          return $state;
        }else{
          return $default;
        }

}