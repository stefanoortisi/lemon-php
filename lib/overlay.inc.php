<?php
function get_cartoons_html(){
	
	ob_start();
?>
	<!-- <img src="<?=l_image( 'manor_cartoons.png' ) ?>" class="title" alt="CARTOONS"/> -->
	<div id="cartoons_slider" class="overlay_slider">
	<?php 
		$cartoons = bat_get_cartoons();
	?>
		<ul>
		<?php 
		foreach( $cartoons as $key => $item ) { 
		if( $key % 3 == 0){ ?> 
		<li>
		<?php } ?>
			<div class="thumb">
				<div class="image">
					<img src="<?=$item[ 'thumb' ] ?>" alt="<?=$item[ 'title' ] ?>"/>
					<a class="fancybox.iframe cartoons_play" href="javascript:;" data-preview="<?=$item[ 'preview' ] ?>" data="<?=$item[ 'video' ] ?>" title="<?=$item[ 'title' ] ?>"><?=$item[ 'title' ] ?></a>
				</div>
				<h1 class="title"><?=$item[ 'title' ] ?></h1>
				<img src="<?=THEME_URL ?>/images/manor/separator_cartoons.png" class="cartoons_separator" />
				<p class="abstract"><?=$item[ 'abstract' ] ?></p>
			</div>
		<?php
		if( $key % 3 == 2){ ?> 
		</li>
		<?php
		}
		?>
		
		<?php } ?>
			
		</ul>
	</div><!-- end cartoons_slider -->
	<div class="carousel-prev">
		<a href="javascript:;" class="arrow_left overlay_arrow" id="prev_cartoons" title="<?=_( 'previous' ) ?>"><?=_( 'previous' ) ?></a>
	</div>
	<div class="carousel-next">
		<a href="javascript:;" class="arrow_right overlay_arrow" id="next_cartoons" title="<?=_( 'next' ) ?>"><?=_( 'next' ) ?></a>
	</div>
	<div class="basetta">
		<p>
			<?=str_replace(
				array( '{actual}', '{total}' ),
				array( '<span class="actual" id="actualCartoons">1</span>', '<span class="total" id="totalCartoons">5</span>'),
				_( 'Page {actual} of {total}' )
			)  ?>
		</p>
	</div>
<?php
	$output = ob_get_clean();
	
	return $output;
}

function get_downloads_html(){
	
	ob_start();
	?>
	<img src="<?=l_image( 'manor_downloads.png' ) ?>" class="title" alt="DOWNLOADS"/>
	<div id="downloads_slider" class="overlay_slider">
	<?php 
		$downloads = bat_get_downloads();
	?>
		<ul>
		<?php 
		foreach( $downloads as $key => $item ) { 

		$size = sizeof( $item[ 'links' ] );
		if( !empty( $item[ 'links' ][ 'pcmac' ] ) ){
			$size = 'pcmac';
		}
		if( $size == 0 ){
			$size = sizeof( $item[ 'link' ] );
		}
		
		if( $key % 3 == 0){ ?> 
		<li>
		<?php } ?>
			<div class="thumb">
				<div class="image">
					<?php if( !empty( $item[ 'link' ] ) ) { ?>
					<a href="<?=$item[ 'link' ][ 1 ] ?>" title="<?=$item[ 'title' ] ?>" ><img src="<?=$item[ 'thumb' ] ?>" alt="<?=$item[ 'title' ] ?>"/></a>
					<?php } else { ?> 
					<img class="lazy" src="<?=$item[ 'thumb' ] ?>" alt="<?=$item[ 'title' ] ?>"/>
					<?php } ?>
				</div>
				<?php if( !empty( $item[ 'link' ] ) ) { ?>
				<h1 class="title"><a href="<?=$item[ 'link' ][1] ?>" title="<?=$item[ 'title' ] ?>" ><?=$item[ 'title' ] ?></a></h1>
				<?php } else { ?> 
				<h1 class="title"><?=$item[ 'title' ] ?></h1>
				<?php } ?>
				
			<?php if( !empty( $item[ 'links' ] )  ) { ?>
				<img src="<?=THEME_URL ?>/images/manor/separator_cartoons.png" class="cartoons_separator" />
				<div class="download_links size_<?=$size ?>">
				<?php if( !empty( $item[ 'link' ] ) ) { ?>
					<a target="_blank" href="<?=$item[ 'link' ][ 1 ] ?>" title="<?=$item[ 'title' ] ?>" class="button53 yellow link">
						<p class="middle"><?=$item[ 'title' ] ?></p>
						<span class="right"></span>
					</a>
				<?php } ?>	
				
				<?php if( !empty( $item[ 'links' ][ 'pc' ] ) ) { ?>
					<a target="_blank" href="<?=$item[ 'links' ][ 'pc' ] ?>" title="<?=_( 'Download for PC' ) ?>" class="">
						<img src="<?=l_image( 'download_pc.png' ) ?>" class="download_button"/>
					</a>
<!-- 					<a href="<?=$link ?>" class="link"><img src="<?=l_image( 'download_pc.png' ) ?>" alt="Download for PC"/></a> -->
				<?php } ?>	
				
				<?php if( !empty( $item[ 'links' ][ 'mac' ] ) ) { ?>
					<a target="_blank" href="<?=$item[ 'links' ][ 'mac' ] ?>" title="<?=_( 'Download for MAC' ) ?>" class="">
						<img src="<?=l_image( 'download_mac.png' ) ?>" class="download_button"/>
					</a>
				<?php } ?>
				<?php if( !empty( $item[ 'links' ][ 'pcmac' ] ) ) { ?>
					<a target="_blank" href="<?=$item[ 'links' ][ 'pcmac' ] ?>" title="<?=_( 'Download for PC & MAC' ) ?>" class="pcmac_wrapper">
						<img src="<?=l_image( 'download_pcmac.png' ) ?>" class="download_button"/>
					</a>
				<?php } ?>	
				</div>
			<?php } ?>
			</div>
		<?php
		if( $key % 3 == 2){ ?> 
		</li>
		<?php
		}
		?>
		
		<?php } ?>
			
		</ul>
	</div><!-- end cartoons_slider -->
	<div class="carousel-prev">
		<a href="javascript:;" class="arrow_left overlay_arrow" id="prev_downloads" title="<?=_( 'previous' ) ?>"><?=_( 'previous' ) ?></a>
	</div>
	<div class="carousel-next">
		<a href="javascript:;" class="arrow_right overlay_arrow" id="next_downloads" title="<?=_( 'next' ) ?>"><?=_( 'next' ) ?></a>
	</div>
	<div class="basetta">
		<p>
			<?=str_replace(
				array( '{actual}', '{total}' ),
				array( '<span class="actual">1</span>', '<span class="total">5</span>'),
				_( 'Page {actual} of {total}' )
			)  ?>
		</p>
	</div>
<?php
	$output = ob_get_clean();
	
	return $output;
}

function get_ecards_html() {
	$cards_info = bat_get_cards();

	ob_start();
	
?>
	<div class="manor_overlay" id="cards_overlay">
		<img src="<?=l_image( 'manor_ecards.png' ) ?>" class="title" alt="ECARDS"/>
		<div class="cards_wrapper">
			<div class="inner">
				<div id="first_screen">
					<div class="ecards_loading">
						<img src="<?=THEME_URL ?>/images/ajax-loader_ecards.gif" />
					</div><!-- end ecards_loading -->
					<div class="confirm">
						<h1><?=_( 'Thank you!<br />your e-card has been sent!' ); ?></h1>
						<a href="<?=BAT_URL_LANG ?>/home/manor/" id="close_ecard" title="<?=_( 'Close' ) ?>">
							<!-- <img src="<?=l_image( 'ecard_close.png' ); ?>" alt="<?=_( 'Close' ) ?>"/> -->
							<span><?=_( 'Close' ) ?></span>
						</a>
					</div>
					<div class="left_section">
						<img src="<?=l_image( 'ecard_choose_card.png' ) ?>" alt="<?=_( 'Choose a card' ); ?>" class="choose_label" />
						<div id="ecard_slider">
							<ul>
							<?php foreach( $cards_info[ 'cards' ] as $card ){ ?>
								<li><img data-mail="<?=$card[ 'mail' ] ?>" src="<?=$card[ 'web' ] ?>" alt="ecard" /></li>
							<?php } ?>
							</ul>
						</div><!--end ecard_slider -->
						<div class="frame" id="frame_left"></div>
						<div class="frame" id="frame_top"></div>
						<div class="frame" id="frame_right"></div>
						<div class="frame" id="frame_bottom"></div>
						
						<div class="carousel-prev">
							<a href="javascript:;" class="little_arrow_left" id="ecard_left"><?=_( 'Left' ); ?></a>
						</div>
						<div class="carousel-next">
							<a href="javascript:;" class="little_arrow_right" id="ecard_right"><?=_( 'Right' ); ?></a>
						</div>
					</div><!-- end left -->
					<img src="<?=THEME_URL?>/images/manor/divisor.png" class="divisor" alt="e-cards"/>
					<div class="right_section">
						<img src="<?=l_image( 'ecard_add_message.png' ) ?>" alt="Add a message" class="choose_label" />
						<form id="ecard_form">
							<fieldset><input data="input_1" type="text" name="name" id="name" class="placeholder-compatibility tocheck_text" placeholder="<?=_( 'Your Name' ); ?>"/></fieldset>
							<?php 
								
							?>
							<fieldset class="nomargin"><input data="input_2" type="text" name="email" id="email" class="placeholder-compatibility tocheck_email" placeholder="<?=_( 'Your e-mail' ) ?>"/></fieldset>
							<fieldset><input data="input_3" type="text" name="friend_name" id="friend_name" class="placeholder-compatibility tocheck_text" placeholder="<?=_( 'Your Friend\'s Name' ) ?>"/></fieldset>
							<fieldset class="nomargin"><input data="input_4" type="text" name="friend_email" id="friend_email" class="placeholder-compatibility tocheck_email" placeholder="<?=_( 'Your Friend\'s e-mail' ) ?>"/></fieldset>
							<fieldset class="multi"><textarea data="input_5" id="message" name="message" class="placeholder-compatibility tocheck_text" placeholder="<?=_( 'Your Message' ) ?>"></textarea></fieldset>
							<a href="javascript:;" class="check">checked</a>
							<label for="check">
								<?=str_replace( 
								   		array( '{link}', '{/link}' ),
										array( '<a id="privacy_link" href="javascript:;" title="'. _('Read the privacy policy' ) . '">', '</a>' ), 
										_( 'Yes, I\'ve read and understood the {link} privacy policy {/link} and <br /> agree with it.' ) 
								) ?>
							</label>
							<a href="javascript:;" id="submit"  class="button58 yellow disabled"><span class="middle"><?=_( 'Send e-card' ); ?></span><span class="right"></span></a>
						</form>
					</div><!-- end right -->
				</div><!-- end first_screen -->
				<div id="second_screen" class="ready">
					<div class="ext_scroll">
						<div class="scroll" id="scroll_ecard">
							<?=$cards_info[ 'privacy' ] ?>
							<br/><br/><br/><br/>
						</div><!-- end scroll -->
					</div>
					<div class="scroll_fade"></div>
					<a href="javascript:;" id="close_privacy" title="<?=_( 'Close' ) ?>"><span><?=_( 'Close' ) ?></span></a>
				</div><!-- end second_screen -->
				
			</div><!-- end inner -->		
		</div><!-- end cards_wrapper -->
		
		
		
	</div><!-- end cards_overlay -->
<?php
	$output = ob_get_clean();
	
	return $output;
}

?>