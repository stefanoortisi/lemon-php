<?php
	function load_res($relPath) {
		print parse_path($relPath);
	}
	
	function parse_path($relPath) {
		return rtrim(dirname($_SERVER['PHP_SELF']), '/') . '/' . ltrim($relPath, '/');
	}
	
	function load_view($view, $data=NULL) {
		$fileName = 'view.' . $view . '.php';
		if (file_exists($fileName)) {
			if (!empty($data)) {
				foreach ($data as $key => $value) {
					$$key = $value;
				}
			}
			include($fileName);
		}
	}
		
	function redirect($url, $message=NULL, $messageClass=NULL) {
		if (!empty($message)) {
			send_message($message, $messageClass);
		}
		header('Location: ' . $url);
		exit();
	}
	
	function log_add($source, $event, $logName='general') {
		$path = PATH_ROOT . '/log/' . $logName . '_' . date('m_Y') . '.log';
		$fh = fopen($path, 'a');
		if ($fh) {
			fwrite($fh, date('Y-m-d H.i.s') . "\t" . $_SERVER['REMOTE_ADDR'] . "\t" . $source . "\t" . $event . "\n");
			fclose($fh);
		}
	}
	
	function _g($arg=NULL, $default=NULL) {
		if (!empty($arg)) {
			return $arg;
		} else {
			return (!empty($default)) ? $default : NULL;
		}
	}
	
	function _p($arg=NULL, $default=NULL) {
		print _g($arg, $default);
	}