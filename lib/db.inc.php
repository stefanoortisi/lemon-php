<?php
	function bat_db_connect() {
		global $db_host, $db_user, $db_pass, $db_name;
		mysql_connect($db_host, $db_user, $db_pass) or db_error();
		mysql_select_db($db_name) or db_error();
		mysql_query('SET NAMES UTF8');
	}
	
	function db_exec($sql, $ret_id=false) {
		bat_db_connect();
		$retval = mysql_query($sql);
		if (!$retval) {
			db_error();
		} else {
			if ($ret_id) {
				global $db_last_insert_id;
				$db_last_insert_id = mysql_insert_id();
			}
		}
		db_close();
		return $retval;
	}
	
	function bat_db_insert_id() {
		global $db_last_insert_id;
		return $db_last_insert_id;
	}
	
	function bat_db_query($sql) {
		bat_db_connect();
		$retval = array();
		$result = mysql_query($sql) or db_error();
		if ($result && mysql_num_rows($result) > 0) {
			while ($row = mysql_fetch_assoc($result)) {
				$retval[] = $row;
			}
		}
		db_close();
		return $retval;
	}
	
	function bat_db_insert($table, $values) {
		$sql = 'INSERT INTO ' . $table . ' (';
		$columns = array();
		$fields = array();
		foreach ($values as $key => $value) {
			$columns[] = $key;
			$fields[] .= "'" .mysql_real_escape_string($value) .  "'";
		}
		$sql .= implode(',', $columns);
		$sql .= ') VALUES (';
		$sql .= implode(',', $fields);
		$sql .= ')';
		return db_exec($sql, true);
	}
	
	function db_delete($table, $ids=NULL, $condition=NULL) {
		$sql = 'DELETE FROM ' . $table . ' WHERE ';
		if (!empty($ids) && is_array($ids)) {
			$sql .= 'id IN (' . implode(',', $ids) . ')';
		}
		else if (!empty($ids)) {
			$sql .= 'id = ' . $ids;
		}
		else if (!empty($condition)) {
			$sql .= $condition;
		}
		else {
			return;
		}
		return db_exec($sql);
	}
	
	function db_error() {
		global $debug;
		log_add('DB', 'DB Error ' . mysql_errno() . ' ' . mysql_error(), 'db');
		if ($debug) {
			// send_response(NULL, 500, 'DB Error ' . mysql_errno() . ' ' . mysql_error());
		} else {
			// send_response(NULL, 500);
		}
	}
	
	function db_close() {
		mysql_close();
	}