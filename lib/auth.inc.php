<?php
	define('AUTH_USER_KEY', '_auth_user');
	
	function auth_is_logged() {
		return (auth_get_user() != NULL);
	}
	
	function auth_set_user($user_data) {
		$_SESSION[AUTH_USER_KEY] = $user_data;
	}
	
	function auth_get_user() {
		if (!empty($_SESSION[AUTH_USER_KEY])) {
			return $_SESSION[AUTH_USER_KEY];
		}
		return NULL;
	}
	
	function auth_login($user, $pass) {
		
	}
	
	function auth_logout() {
		session_destroy();
		session_start();
	}