<?php
	function manor_ctlmain($args) {
		$args[ 'share_info' ] = bat_get_share_info( 'manor' );	
		$args[ 'id_body' ]	= 'manor';
		$args[ 'back_button' ] = 'back_outside';
		$args[ 'back_link' ] = BAT_URL_LANG . '/home/';
		$args[ 'debug' ] = 'new_manor.jpg';
		$args[ 'share_panels' ] = bat_get_share_links();
		load_view('manor', $args);
	}
	
	function manor_infocartoons( $args ){
		send_response( get_cartoons_html() );
	}
	
	function manor_infodownloads( $args ){
		send_response( get_downloads_html() );
	}
	
	function manor_infoecards( $args ){
		send_response( get_ecards_html() );
	}
	
	function manor_infoall( $args ){
		send_response( array(
			'cartoons' => get_cartoons_html(),
			'downloads' => get_downloads_html(),
			'ecards' => get_ecards_html()
		));
	}
	
	function bat_get_share_links(){
		return array(
			'cartoons' => array( 
				'link' => 'http://' . $_SERVER[ 'HTTP_HOST' ] . '/'. BAT_LANG . '/home/manor/cartoons/',
				'others' => bat_get_share_info( 'cartoons' )
			),
			'downloads' => array( 
				'link' => 'http://' . $_SERVER[ 'HTTP_HOST' ] . '/'. BAT_LANG . '/home/manor/downloads/',
				'others' => bat_get_share_info( 'downloads' )
			),
			'cards' => array( 
				'link' => 'http://' . $_SERVER[ 'HTTP_HOST' ] . '/'. BAT_LANG . '/home/manor/cards/',
				'others' => bat_get_share_info( 'cards' )
			),
		);	
	}
	
	
	function manor_cards($args){
		$args[ 'share_info' ] = bat_get_share_info( 'ecards' );	
		$args[ 'debug' ] = 'new_ecards.jpg';
		$args[ 'additional_info' ] = array(
			'ecards' => bat_get_cards()
		);
		_load_section( $args, 'cards' );
	}
	
	function manor_video( $args ){
		$args[ 'share_info' ] = bat_get_share_info( 'video' );	
		$args[ 'id_body' ]	= 'manor';
		$args[ 'back_button' ] = 'back_outside';
		$args[ 'back_link' ] = BAT_URL_LANG . '/home/';
		
		load_view( 'video', $args );
	}
	
	
	function manor_cartoons($args){
		$args[ 'share_info' ] = bat_get_share_info( 'cartoons' );	
		$args[ 'additional_info' ] = array(
			'cartoons' => bat_get_cartoons()
		);
		$args[ 'debug' ] = 'new_cartoons.jpg';
		_load_section( $args, 'cartoons' );
	}
	
	function manor_downloads($args){
		$args[ 'share_info' ] = bat_get_share_info( 'downloads' );	
		$args[ 'additional_info' ] = array(
			'downloads' => bat_get_downloads()
		);
		$args[ 'debug' ] = 'new_downloads.jpg';
		_load_section( $args, 'downloads' );
	}
	
	function _load_section( $args, $section = false ){
		$args[ 'id_body' ]	= 'manor';
		$args[ 'back_button' ] = 'back_home';
		$args[ 'back_link' ] = BAT_URL_LANG . '/home/manor/';
		$args[ 'section' ] = $section;
		
		load_view('manor', $args);
	}
	
	function manor_invia($args) {	
		if ( !empty($args['sender_name']) && !empty($args['sender_email']) && !empty($args['receiver_name']) &&
			!empty($args['receiver_email']) && !empty($args['message']) && !empty($args['image']) && !empty($args['imageMail']) ) {
			
			$to_translate = _( 'Hi {receiver}, {sender} has sent you an e-card!' );
			$title = str_replace( 
				'{receiver}', 
				$args['receiver_name'], 
				$to_translate
			);
			
			$title = str_replace( '{sender}', $args['sender_name'], $title );
			
			$newECard = array(
				'sender_name' => ucwords( $args['sender_name'] ),
				'sender_email' => $args['sender_email'],
				'receiver_name' => ucwords( $args['receiver_name'] ),
				'receiver_email' => $args['receiver_email'],
				'message' => $args['message'],
				'image_url' => $args[ 'imageMail' ],
				'ip' => $_SERVER['REMOTE_ADDR']
			);
			
			//send_response( $newECard );die();
			
			if (bat_db_insert('ecards', $newECard)) {
				$id = bat_db_insert_id();
				$newECard[ 'base_url' ] = 'http://' . $_SERVER[ 'HTTP_HOST' ];
				$newECard[ 'title' ] = $title;
				
				if (_send_mail($id, $newECard)) {
					send_response('OK');
				} else {
					send_response('Impossibile inviare il messaggio.');
				}
			} else {
				send_response('Si è verificato un errore.', 500);
			}
		} else {
			send_response('Dati mancanti.' . print_r($args, true) . ' --> ' . $_SESSION['USER_ID'], 500);
		}
	}
	
	function _send_mail($letter_id, $args) {
		
		require BASE_PATH . '/home/trdparty/phpmailer/class.phpmailer.php';
		$mail = new PHPMailer(true);
		try {
			//$args['letter_link'] = 'http://' . $_SERVER['HTTP_HOST'] . parse_path('lettera/leggi/' . md5($letter_id));
			$args[ 'host_name' ] = $_SERVER[ 'HTTP_HOST' ];
			
			$mail_body = file_get_contents(BASE_PATH . '/home/templates/mail.html');
			foreach ($args as $key => $value) {
				$mail_body = str_replace('%' . $key . '%', $value, $mail_body);
			}
			
			$mail->AddAddress($args['receiver_email'], $args['receiver_name'] );
			$mail->SetFrom(MAIL_ADDRESS, _utf8_to_iso( $args['sender_name'] ) );
			$mail->Subject = _( 'Batpat - Ecard' );
			//$mail->MsgHTML(iconv("UTF-8", "ISO-8859-1", $mail_body));
			//$mail->MsgHTML( _utf8_to_iso( $mail_body ) );
			$mail->MsgHTML( $mail_body );
			$mail->Send();
		} catch (phpmailerException $e) {
			log_add('MAILER', $e->errorMessage(), 'mailer');
			return false;
		} catch (Exception $e) {
			log_add('MAILER', $e->getMessage(), 'mailer');
			return false;
		}
		return true;
	}
	
	function _utf8_to_iso( $string ){
		$search =  array('à', 'è', 'ì'. 'ò', 'ù', 'é' );
		$replace = array('&agrave;', '&egrave;', '&igrave;', '&ograve;', '&ugrave;', '&eacute;' );
		$string =  str_replace($search, $replace, $string);
		
		return $string;
	}
	