<?php
	function overlay_ctlmain($args) {
		
		$args[ 'id_body' ]	= 'overlay';
		load_view('overlay', $args);
	}
	
	function overlay_legal($args) {
		$args[ 'id_body' ]	= 'overlay';
		$args[ 'page' ] = 'legal';
		load_view('overlay', $args);
	}
	
	function overlay_credits($args) {
		$args[ 'id_body' ]	= 'overlay';
		$args[ 'page' ] = 'credits';
		load_view('overlay', $args);
	}
	
	function overlay_contacts($args) {
		$args[ 'id_body' ]	= 'overlay';
		$args[ 'page' ] = 'contacts';
		
		load_view('overlay', $args);
	}
	
	function overlay_news($args) {
		$args[ 'id_body' ]	= 'overlay';
		$args[ 'page' ] = 'news';
		$args[ 'news' ] = bat_get_news();
		
		load_view('overlay', $args);
	}
	
	function _get_news(){
		
		return array(
			'title' => 'Bat Pat at Bologna Children\'s Book Fair!',
			'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, nisi ut aliquip ex ea commodo consequat.<br/><br/>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur…',
			'link' => 'javascript:;',
			'image' => THEME_URL . '/images/ui/testnews.jpg'
		);
	}
?>